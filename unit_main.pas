unit unit_main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, XMLPropStorage,
  ActnList, Menus, ComCtrls, StdActns, StdCtrls, Buttons, ExtCtrls;

type

  { TFormMain }

  TFormMain = class(TForm)
    ActionAbout: TAction;
    ActionConfigSelectTemplatePath: TAction;
    ActionConfigSelectApacheSitesAvailablePath: TAction;
    ActionConfigSelectApacheEnvVarFile: TAction;
    ActionConfigSelectWorkspaceRoot: TAction;
    ActionProjectUpdateApache: TAction;
    ActionProjectSelect: TAction;
    ActionProjectCreate: TAction;
    ActionList: TActionList;
    BtnUpdateApache: TBitBtn;
    BtnCreateProject: TBitBtn;
    BtnSelectProject: TBitBtn;
    BtnTemplatePath: TBitBtn;
    BtnWorkspaceRoot: TBitBtn;
    BtnApacheEnvVarsFile: TBitBtn;
    BtnApacheSitesAvailablePath: TBitBtn;
    CgApplyUpdates: TCheckGroup;
    CgSystem: TCheckGroup;
    EdApacheEnvVarsFile: TEdit;
    EdApacheSitesAvailablePath: TEdit;
    EdApacheServerAdmin: TEdit;
    EdProjectName: TEdit;
    EdWorkspaceRoot: TEdit;
    EdTemplatesPath: TEdit;
    FileExit1: TFileExit;
    GbApacheConfig: TGroupBox;
    GbWorkSpace: TGroupBox;
    GbProject: TGroupBox;
    GbVirtualServerConfFile: TGroupBox;
    LabelApacheServerAdmin: TLabel;
    LabelApacheEnvVarsFile: TLabel;
    LabelApacheSitesAvailablePath: TLabel;
    LabelNewProject: TLabel;
    LabelTemplatePath: TLabel;
    LabelWorkspacePath: TLabel;
    MainMenu: TMainMenu;
    MemoLog: TMemo;
    MemoVirtualServerConfig: TMemo;
    MenuFile: TMenuItem;
    MenuHelp: TMenuItem;
    MenuItemAbout: TMenuItem;
    MenuItemFileExit: TMenuItem;
    OpenDialog: TOpenDialog;
    PageControl1: TPageControl;
    SelectDirectoryDialog: TSelectDirectoryDialog;
    TsLog: TTabSheet;
    TsProject: TTabSheet;
    TsConfig: TTabSheet;
    XMLPropStorage: TXMLPropStorage;
    procedure ActionAboutExecute(Sender: TObject);
    procedure ActionConfigSelectApacheEnvVarFileExecute(Sender: TObject);
    procedure ActionConfigSelectApacheSitesAvailablePathExecute(Sender: TObject
      );
    procedure ActionConfigSelectTemplatePathExecute(Sender: TObject);
    procedure ActionConfigSelectWorkspaceRootExecute(Sender: TObject);
    procedure ActionProjectCreateExecute(Sender: TObject);
    procedure ActionProjectSelectExecute(Sender: TObject);
    procedure ActionProjectUpdateApacheExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TsConfigShow(Sender: TObject);
    procedure TsProjectShow(Sender: TObject);
  private
    procedure AppendLog(const S: string);
    function SudoExecute(const Command: string; const Password: string): integer;
    function GetPropStorageValue(Form: TForm; ValueName: string; DefaultValue: string): string;
    procedure RestoreConfigValues;
    procedure StoreConfigValues;
    procedure UpdateVirtualserverConfig;
  public
    procedure RestoreFormState(Form: TForm);
    procedure StoreFormState(Form: TForm);
  end;

var
  FormMain: TFormMain;

implementation
uses StrUtils, Process, Math,
  unit_password, unit_about;
{$R *.lfm}

{ TFormMain }

procedure TFormMain.FormCreate(Sender: TObject);
begin
  EdApacheEnvVarsFile.Text := XMLPropStorage.StoredValue['ApacheEnvVarsFile'];
  EdApacheSitesAvailablePath.Text := XMLPropStorage.StoredValue['ApacheSitesAvailablePath'];
  EdTemplatesPath.Text := XMLPropStorage.StoredValue['TemplatePath'];
  if EdTemplatesPath.Text = '' then
  begin
    EdTemplatesPath.Text :=  ExtractFilePath(Application.ExeName) + 'Templates';
  end;
  EdWorkspaceRoot.Text := trim(XMLPropStorage.StoredValue['WorkspaceRoot']);
  EdApacheServerAdmin.Text := XMLPropStorage.StoredValue['ApacheServerAdmin'];
end;

procedure TFormMain.FormShow(Sender: TObject);
begin
  RestoreFormState(Self);
  RestoreConfigValues;
end;

procedure TFormMain.TsConfigShow(Sender: TObject);
begin
  FormMain.Height := 508;
end;

procedure TFormMain.TsProjectShow(Sender: TObject);
begin
  FormMain.Height := 617;
end;

procedure TFormMain.AppendLog(const S: string);
begin
  MemoLog.Lines.Add(S);
end;

function TFormMain.SudoExecute(const Command: string; const Password: string): integer;
var
  Proc: TProcess;
  CharBuffer: array [0..511] of char;
  ReadCount: integer;
  ExitCode: integer;
  SudoPassword: string;
begin
  SudoPassword := Password;
  ExitCode := -1; //Start out with failure, let's see later if it works
  Proc := TProcess.Create(nil); //Create a new process
  try
    Proc.Options := [poUsePipes, poStderrToOutPut]; //Use pipes to redirect program stdin,stdout,stderr
    Proc.CommandLine := 'sudo -S ' + Command; //Run ls /root as root using sudo
//    Proc.CommandLine := 'sudo -S ls /root'; //Run ls /root as root using sudo
    // -S causes sudo to read the password from stdin.
    Proc.Execute; //start it. sudo will now probably ask for a password

    // write the password to stdin of the sudo program:
    SudoPassword := SudoPassword + LineEnding;
    Proc.Input.Write(SudoPassword[1], Length(SudoPassword));
    SudoPassword := '%*'; //short string, hope this will scramble memory a bit; note: using PChars is more fool-proof
    SudoPassword := ''; // and make the program a bit safer from snooping?!?

    // main loop to read output from stdout and stderr of sudo
    while Proc.Running
       or (Proc.Output.NumBytesAvailable > 0) do
//       or (Proc.Stderr.NumBytesAvailable > 0) do
    begin
      // read stdout and write to our stdout
      while Proc.Output.NumBytesAvailable > 0 do
      begin
        ReadCount := Min(512, Proc.Output.NumBytesAvailable); //Read up to buffer, not more
        Proc.Output.Read(CharBuffer, ReadCount);
        MemoLog.Lines.Add (Copy(CharBuffer, 0, ReadCount));
//        Write(StdOut, Copy(CharBuffer, 0, ReadCount));
      end;
      // read stderr and write to our stderr
//      while Proc.Stderr.NumBytesAvailable > 0 do
//      begin
//        ReadCount := Min(512, Proc.Stderr.NumBytesAvailable); //Read up to buffer, not more
//        Proc.Stderr.Read(CharBuffer, ReadCount);
//        MemoLog.Lines.Add ('[err] ' + Copy(CharBuffer, 0, ReadCount));
//        Write(StdErr, Copy(CharBuffer, 0, ReadCount));
//      end;
    end;
    ExitCode := Proc.ExitStatus;
  finally
    Proc.Free;
    result := ExitCode;
//    Halt(ExitCode);
  end;
end;

function TFormMain.GetPropStorageValue(Form: TForm; ValueName: string;
  DefaultValue: string): string;
begin
  if XMLPropStorage.StoredValue[Form.Name + '.' + ValueName] = '' then
     Result := DefaultValue
  else
     Result := XMLPropStorage.StoredValue[Form.Name + '.' + ValueName];
end;

procedure TFormMain.RestoreConfigValues;
begin
  EdApacheServerAdmin.Text := XMLPropStorage.StoredValue['ApacheServerAdmin'];
  EdApacheSitesAvailablePath.Text := XMLPropStorage.StoredValue['ApacheSitesAvailablePath'];
  EdApacheEnvVarsFile.Text := XMLPropStorage.StoredValue['ApacheEnvVarsFile'];
  EdTemplatesPath.Text := XMLPropStorage.StoredValue['TemplatesPath'];
  EdWorkspaceRoot.Text := XMLPropStorage.StoredValue['WorkspaceRoot'];
end;

procedure TFormMain.StoreConfigValues;
begin
  XMLPropStorage.StoredValue['ApacheServerAdmin'] := EdApacheServerAdmin.Text;
  XMLPropStorage.StoredValue['ApacheSitesAvailablePath'] := EdApacheSitesAvailablePath.Text;
  XMLPropStorage.StoredValue['ApacheEnvVarsFile'] := EdApacheEnvVarsFile.Text;
  XMLPropStorage.StoredValue['TemplatesPath'] := EdTemplatesPath.Text;
  XMLPropStorage.StoredValue['WorkspaceRoot'] := EdWorkspaceRoot.Text;
end;

procedure TFormMain.UpdateVirtualserverConfig;
var
  Command: string;
  ConfigFilename: string;
  ProjectRoot: string;
  Doupdate: boolean;
  I: integer;
  Password: string;
  Res: Ansistring;
  TempFile: TStringList;
begin
  Password := '';
  Doupdate := false;
  TempFile := nil;

  ProjectRoot := ExpandFilename(EdWorkspaceRoot.Text) + '/' + EdProjectName.Text;
  if not DirectoryExists(ProjectRoot) then
  begin
    CreateDir(ProjectRoot);
  end;

  if not DirectoryExists(ProjectRoot + '/Log') then
  begin
    CreateDir(ProjectRoot + '/Log');
  end;

  if not DirectoryExists(ProjectRoot + '/Web') then
  begin
    CreateDir(ProjectRoot + '/Web');
  end;

  if not DirectoryExists(ProjectRoot + '/Web/public') then
  begin
    CreateDir(ProjectRoot + '/Web/public');
  end;

  if not FileExists(ProjectRoot + '/Web/public/index.html') then
  begin
    TempFile := TStringList.Create;
    try
      TempFile.LoadFromFile(ExpandFileName(EdTemplatesPath.Text) + '/index.html');
      TempFile.Text := ReplaceStr(TempFile.Text,'#ProjectName#',EdProjectName.Text);;
      TempFile.SaveToFile(ProjectRoot + '/Web/public/index.html');
    finally
      TempFile.Free;
    end;
  end;

  if not FileExists(ProjectRoot + '/Web/public/index.php') then
  begin
    TempFile := TStringList.Create;
    try
      TempFile.LoadFromFile(ExpandFileName(EdTemplatesPath.Text) + '/index.php');
      TempFile.Text := ReplaceStr(TempFile.Text,'#ProjectName#',EdProjectName.Text);;
      TempFile.SaveToFile(ProjectRoot + '/Web/public/index.php');
    finally
      TempFile.Free;
    end;
  end;


  for I := 0 to 3 do
  begin
    Doupdate := Doupdate or CgApplyUpdates.Checked[I];
  end;

  if Doupdate and (FormPassword.ShowModal = mrOK) then
  begin

    ConfigFilename := ExpandFilename(EdTemplatesPath.Text) + '/' + EdProjectName.Text + '.conf';

    if CgApplyUpdates.Checked[0] then
    begin
      MemoVirtualServerConfig.Lines.SaveToFile(ConfigFilename);
      Command := 'cp ' + ConfigFilename + ' ' + EdApacheSitesAvailablePath.Text + '/';
      SudoExecute(Command,FormPassword.EdPassword.Text);
      Deletefile(ConfigFilename);
    end;

    if CgApplyUpdates.Checked[1] then
    begin
      Command := 'a2ensite ' + EdProjectName.Text + '.conf';
      SudoExecute(Command,FormPassword.EdPassword.Text);
    end;

    if CgApplyUpdates.Checked[2] then
    begin
      TempFile := TStringList.Create;
      try
        TempFile.LoadFromFile('/etc/hosts');
        TempFile.Add('# added by LAMPDeveloperUtil');
        TempFile.Add('127.0.0.1 ' +  EdProjectName.Text +'.local');
        TempFile.SaveToFile(ExpandFilename(EdTemplatesPath.Text) + '/hosts');
        Command := 'cp ' + ExpandFilename(EdTemplatesPath.Text) + '/hosts /etc/hosts';
        SudoExecute(Command,FormPassword.EdPassword.Text);
        Deletefile(ExpandFilename(EdTemplatesPath.Text) + '/hosts');
      finally
        TempFile.Free;
      end;
    end;

    if CgApplyUpdates.Checked[3] then
    begin
      Command := 'systemctl reload apache2';
      SudoExecute(Command,FormPassword.EdPassword.Text);
    end;

    RunCommand('ln',['-s','/var/log/apache2/' + EdProjectName.Text + '-access.log', ProjectRoot + '/Log/access.log'],Res);
    RunCommand('ln',['-s','/var/log/apache2/' + EdProjectName.Text + '-error.log', ProjectRoot + '/Log/error.log'],Res);

  end;
end;

procedure TFormMain.RestoreFormState(Form: TForm);
begin
  Form.Top := StrToInt(GetPropStorageValue(Form,'Top','100'));
  Form.Left := StrToInt(GetPropStorageValue(Form,'Left','100'));
end;

procedure TFormMain.StoreFormState(Form: TForm);
begin
  XMLPropStorage.StoredValue[Form.Name + '.Top'] := IntToStr(Form.Top);
  XMLPropStorage.StoredValue[Form.Name + '.Left'] := IntToStr(Form.Left);
end;

procedure TFormMain.ActionProjectCreateExecute(Sender: TObject);
var
  TemplateFile: string;
  DocRoot: string;
  ServerAdmin: string;
begin
  TemplateFile := EdTemplatesPath.Text + '/apache-virtual-server.conf';
  DocRoot := ExpandFilename(EdWorkspaceRoot.Text) + '/' + EdProjectName.Text + '/Web/public';
  if EdApacheServerAdmin.Text <> '' then
  begin
    ServerAdmin := EdApacheServerAdmin.Text;
  end
  else
  begin
    ServerAdmin := 'info@' + EdProjectName.Text;
  end;
  MemoVirtualServerConfig.Lines.LoadFromFile(TemplateFile);
  MemoVirtualServerConfig.Text := ReplaceStr(MemoVirtualServerConfig.Text,'#ProjectName#',EdProjectName.Text);
  MemoVirtualServerConfig.Text := ReplaceStr(MemoVirtualServerConfig.Text,'#DocumentRoot#',DocRoot);
  MemoVirtualServerConfig.Text := ReplaceStr(MemoVirtualServerConfig.Text,'#ServerAdmin#',ServerAdmin);
end;

procedure TFormMain.ActionConfigSelectApacheEnvVarFileExecute(Sender: TObject);
begin
  if EdApacheEnvVarsFile.Text <> '' then
  begin
    OpenDialog.FileName := EdApacheEnvVarsFile.Text;
  end
  else
  begin
    OpenDialog.Filename := '/etc/apache2';
  end;
  if OpenDialog.Execute then
  begin
    EdApacheEnvVarsFile.Text := OpenDialog.Filename;
  end;
end;

procedure TFormMain.ActionAboutExecute(Sender: TObject);
begin
  FormAbout.ShowModal;
end;

procedure TFormMain.ActionConfigSelectApacheSitesAvailablePathExecute(
  Sender: TObject);
begin
  if EdApacheSitesAvailablePath.Text <> '' then
  begin
    SelectDirectoryDialog.FileName := EdApacheSitesAvailablePath.Text;
  end
  else
  begin
    SelectDirectoryDialog.Filename := '/etc/apache2';
  end;
  if SelectDirectoryDialog.Execute then
  begin
    EdApacheSitesAvailablePath.Text := SelectDirectoryDialog.Filename;
  end;
end;

procedure TFormMain.ActionConfigSelectTemplatePathExecute(Sender: TObject);
begin
  if EdTemplatesPath.Text <> '' then
  begin
    SelectDirectoryDialog.FileName := EdTemplatesPath.Text;
  end
  else
  begin
    SelectDirectoryDialog.FileName := ExtractFilePath(Application.ExeName);
  end;
  if SelectDirectoryDialog.Execute then
  begin
    EdTemplatesPath.Text := SelectDirectoryDialog.Filename;
  end;

end;

procedure TFormMain.ActionConfigSelectWorkspaceRootExecute(Sender: TObject);
begin
  SelectDirectoryDialog.FileName := EdWorkspaceRoot.Text;
  if SelectDirectoryDialog.Execute then
  begin
    EdWorkspaceRoot.Text := SelectDirectoryDialog.Filename;
  end;
end;

procedure TFormMain.ActionProjectSelectExecute(Sender: TObject);
begin
  SelectDirectoryDialog.FileName := EdWorkspaceRoot.Text;
  if SelectDirectoryDialog.Execute then
  begin
    EdProjectName.Text := ExtractFilename(SelectDirectoryDialog.Filename);
  end;
end;

procedure TFormMain.ActionProjectUpdateApacheExecute(Sender: TObject);
begin
  UpdateVirtualserverConfig
end;

procedure TFormMain.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
    StoreConfigValues;
    StoreFormState(Self);
end;


end.

