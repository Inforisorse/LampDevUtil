unit unit_about;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  Buttons, IpHtml;

type

  { TFormAbout }

  TFormAbout = class(TForm)
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Image1: TImage;
    Image2: TImage;
    Image3: TImage;
    HtmlPanelLicense: TIpHtmlPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MemoLicense: TMemo;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  FormAbout: TFormAbout;

implementation

{$R *.lfm}

{ TFormAbout }

procedure TFormAbout.FormShow(Sender: TObject);
var
  fs: TStringStream;
  pHTML: TIpHtml;
begin
  if MemoLicense.Text <> '' then
  begin
    try
      fs := TStringStream.Create( MemoLicense.Text );
      try
        pHTML:=TIpHtml.Create; // Beware: Will be freed automatically by IpHtmlPanel1
        pHTML.LoadFromStream(fs);
      finally
        fs.Free;
      end;
      HtmlPanelLicense.SetHtml( pHTML );
      //Caption := IpHtmlPanelLicense.Title;
    except
      on E: Exception do begin
        MessageDlg( 'Error: '+E.Message, mtError, [mbCancel], 0 );
      end;
    end;
    MemoLicense.Clear;
  end;
end;

end.

